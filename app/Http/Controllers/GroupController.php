<?php

namespace App\Http\Controllers;

use App\User;
use App\Group;
use App\GroupUser;

use Illuminate\Http\Request;

class GroupController extends Controller
{
    public function show(User $user, Group $group)
    {
        $isPrivate = $group->isPrivate;
        $pivotRow = $user->groups->where("group_id", "=", $group->id);

        // {SAFETY} EQUAL ONE ONLY AS WE REFERENCE THE PIVOT TABLE
        if($pivotRow->count() === 1)
        {
            // GET FIRST ELEMENT
            $isAdmin = $pivotRow->shift()->isAdmin;

            // {SAFETY}
            if(!$isPrivate || $isAdmin)
            {
                return response()->json([
                  'success' => "",
                  'code' => "200",
                  'data' => [
                    'group' => $group,
                    'admins' => $group->users->where("isAdmin", "=", "1"),
                    'members' => $group->users->where("isAdmin", "=", "0")]], 200);
            }
            else
            {
                return response()->json(['error' => 'Insufficient Account Permissions', 'code' => '403'], 403);
            }
        }

        return response()->json(['error' => 'Invalid Uri', 'code' => '400'], 400);
    }

    public function update(Request $request, User $user, Group $group)
    {
        $liveGroup = Group::findOrFail($group->id);
        $pivotRow = $user->groups->where("group_id", "=", $group->id);

        if($pivotRow->count() === 1)
        {
            $isAdmin = $pivotRow->shift()->isAdmin;

            if($isAdmin)
            {
                $rules = [
                    'title' => 'min:1',
                    'description' => 'min:1'
                ];

                $this->validate($request, $rules);

                if($request->has("title"))
                {
                    $liveGroup->title = $request->title;
                }

                if($request->has("description"))
                {
                    $liveGroup->description = $request->description;
                }

                $liveGroup->save();

                return response()->json(['success' => '', 'code' => '201', 'data' => ['message' => 'Group updated successfully']], 201);
            }
            else
            {
                return response()->json(['error' => 'Insufficient Account Permissions', 'code' => '403'], 403);
            }
        }

        return response()->json(['error' => 'Invalid Uri', 'code' => '400'], 400);
    }
}
