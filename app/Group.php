<?php

namespace App;

use App\User;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    // WE DO NOT CARE FOR TIMESTAMPS
    public $timestamps = false;

    // ALLOW THESE TABLE COLUMNS TO BE MASS ASSIGNED USING ELOQUENT
    protected $fillable = ['title', 'description', 'isPrivate'];

    // ONE-TO-MANY ASSOCIATION WITH THE USER TABLE
    public function users()
    {
        return $this->hasMany(GroupUser::class);
    }
}
