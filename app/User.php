<?php

namespace App;

use App\Group;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    // ALLOW THESE TABLE COLUMNS TO BE MASS ASSIGNED USING ELOQUENT
    protected $fillable = [
        'name', 'email', 'password',
    ];

    // ALLOW THESE TABLE COLUMNS TO BE MASS ASSIGNED USING ELOQUENT
    protected $hidden = [
        'password', 'remember_token',
    ];

    // ONE-TO-MANY ASSOCIATION WITH THE USER TABLE
    public function groups()
    {
        return $this->hasMany(GroupUser::class);
    }

    public function groupsAdmin()
    {
        return $this->hasMany(GroupUser::class);
    }
}
