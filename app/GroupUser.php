<?php

namespace App;

use App\Group;
use App\User;
use App\GroupUser;

use Illuminate\Database\Eloquent\Model;

class GroupUser extends Model
{
    // ALLOW THESE TABLE COLUMNS TO BE MASS ASSIGNED USING ELOQUENT
    protected $fillable = ['group_id', 'user_id', 'isAdmin'];

    // ONE-TO-ONE ASSOCIATIONS
    public function Group()
    {
        return $this->belongsTo(Group::class);
    }

    public function User()
    {
        return $this->belongsTo(User::class);
    }
}
