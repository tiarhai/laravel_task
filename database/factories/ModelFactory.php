<?php

use App\User;
use App\Group;
use App\GroupUser;

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10)
    ];
});

$factory->define(Group::class, function (Faker $faker) {

    return [
      'title' => $faker->catchPhrase,
      'description' => $faker->paragraph,
      'isPrivate' => $faker->boolean($chanceOfGettingTrue = 50)
    ];
});

$factory->define(GroupUser::class, function (Faker $faker) {

    $group = Group::all()->random();
    $user = User::all()->random();

    return [
      'group_id' => $group->id,
      'user_id' => $user->id,
      'isAdmin' => $faker->boolean($chanceOfGettingTrue = 20)
    ];
});
