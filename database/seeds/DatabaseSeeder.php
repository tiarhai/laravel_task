<?php

use App\User;
use App\Group;
use App\GroupUser;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // TEMP DISABLE FOREIGN KEYS
        DB::statement("SET FOREIGN_KEY_CHECKS = 0");

        // REMOVE ALL PREVIOUS DATA
        User::truncate();
        Group::truncate();
        GroupUser::truncate();

        // FACTORY VARS
        $userQuantity = 300;
        $groupQuantity = 20;
        $groupUserQuantity = 3000;

        // CREATE SEEDS USING FACTORY
        factory(User::class, $userQuantity)->create();
        factory(Group::class, $groupQuantity)->create();
        factory(GroupUser::class, $groupUserQuantity)->create();
    }
}
