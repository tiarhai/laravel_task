<?php

Use App\Group;
Use App\GroupUser;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('{user}/group/{group}', 'GroupController@show');
Route::put('{user}/group/{group}', 'GroupController@update');
